# GDE Greeter

This is a lightdm greeter designed for GDE

I referenced [lightdm-elephant-greeter](https://github.com/max-moser/lightdm-elephant-greeter)
to write the lightdm bits of this greeter.

### TODO
- Non 24-h clock
- Fix crashes w/ typing password @ shield quickly (maybe just need new libadwaita?)
- Fix crashes w/ clicking on profile pic (maybe just need new libadwaita?)
- Fix guest session
- Handle all the prompt types
- Fixup the power button
- Figure out why spice-vdagent throws a fit in GNOME Boxes

