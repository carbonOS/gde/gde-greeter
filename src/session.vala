private async void do_spawn(string cmd, bool force) {
    print("Starting: %s\n", cmd);
    bool success = false;
    try {
        var proc = new Subprocess.newv({"systemd-cat", cmd },
            SubprocessFlags.INHERIT_FDS);
        success = yield proc.wait_check_async();
    } catch (Error e) {
        critical(e.message);
    }
    print("%s: %s\n", success ? "Exited" : "FAILED", cmd);
    if (!success || force) do_spawn.begin(cmd, force);
}

private void run(string command, bool libexec = false, bool force = true) {
    var prefix = libexec ? "/usr/libexec/" : "";
    do_spawn.begin(prefix + command, force);
}

void main() {
    // Look nice in GNOME Boxes
    run("spice-vdagent", false, false /* allow it to exit */);

    // Auto-manage displays
    run("gde-gnome-bridge", true);

    // End session dialog
    run("gde-end-session-dialog-service", true);

    // Wallpaper
    run("gde-background");

    // Run the appropriate target (either the greeter or initial-setup)
    if (LightDM.UserList.get_instance().length > 0)
        run("gde-greeter");
    else {
        run("carbon-setup");
        run("gde-panel");
    }

    // Run forever
    new MainLoop().run();
}
