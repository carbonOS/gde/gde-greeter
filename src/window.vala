[GtkTemplate(ui="/sh/carbon/gde-greeter/ui/window.ui")]
class Greeter.Window : Adw.Window {
    [GtkChild] unowned Adw.Carousel main_switcher;
    [GtkChild] unowned Gtk.Button slide_arrow_btn;
    [GtkChild] unowned Gtk.Label clock;
    [GtkChild] unowned Gtk.Label date;
    [GtkChild] unowned Gtk.Widget bat_root;
    [GtkChild] unowned Gtk.Image bat_icon;
    [GtkChild] unowned Gtk.Label bat_percent;
    [GtkChild] unowned Adw.Carousel user_carousel;
    [GtkChild] unowned Gtk.Box password_box;
    [GtkChild] unowned Gtk.PasswordEntry password_entry;
    [GtkChild] unowned Gtk.Button login_button;
    [GtkChild] unowned Gtk.Button emergency_btn;

    private Gtk.CssProvider bg_provider;
    private Gtk.IconTheme icon_theme;

    private GreeterImpl greeter;

    construct {
        // Setup LightDM
        greeter = new GreeterImpl();
        greeter.prompt.connect(on_greeter_prompt);
        greeter.success.connect(on_greeter_success);
        greeter.failed.connect(on_greeter_failed);

        // Create the user tiles
        var user_list = LightDM.UserList.get_instance();
        user_list.user_added.connect(it => populate_users());
        user_list.user_removed.connect(it => populate_users());
        populate_users();

        // Setup CSS context for bg
        bg_provider = new Gtk.CssProvider();
        main_switcher.get_style_context().add_provider(bg_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        // Setup the carousels
        on_main_switcher_changed(0);
        main_switcher.notify["position"].connect(on_shield_progress_changed);
        on_shield_progress_changed();
        user_carousel.notify["position"].connect(on_user_progress_changed);
        on_user_progress_changed();

        // Setup the clock
        on_clock_tick();
        Timeout.add(250, on_clock_tick);

        // Setup the battery info
        icon_theme = Gtk.IconTheme.get_for_display(Gdk.Display.get_default());
        try {
            var device = Bus.get_proxy_sync<UPower.Device?>(BusType.SYSTEM,
                "org.freedesktop.UPower",
                "/org/freedesktop/UPower/devices/DisplayDevice");
            device.g_properties_changed.connect((a, b) => {
                update_bat_info(device);
            });
            update_bat_info(device);
        } catch (Error e) {
            warning("Failed to set-up battery info: %s", e.message);
            bat_root.visible = false;
        }

        // Hide the emergency button if calls isn't available
        emergency_btn.visible = (new DesktopAppInfo("sm.puri.Calls") != null);

        // Show the window
        fullscreen();
        show();
    }

    // Shield contents

    private bool on_clock_tick() {
        var now = new DateTime.now_local();
        clock.label = now.format("%R");
        // TODO: Non 24-hour clock. Maybe make this a per-user setting?
        date.label = now.format("%B %-e, %Y");
        return Source.CONTINUE;
    }

    private void update_bat_info(UPower.Device dev) {
        if (!dev.is_present || dev.Type != UPower.DeviceType.BATTERY) {
            bat_root.visible = false;
            return;
        }
        bat_root.visible = true;

        // Show the battery percentage
        bat_percent.label = "%0.lf%%".printf(dev.percentage);

        // Try to use the fine battery icons
        var level = 10 * Math.floor(dev.percentage / 10);
        var charging = (dev.state == UPower.DeviceState.CHARGING) ?
            "-charging" : "";
        if (dev.state == UPower.DeviceState.FULLY_CHARGED)
            bat_icon.icon_name = "battery-level-100-charged-symbolic";
        else
            bat_icon.icon_name = "battery-level-%0.f%s-symbolic".printf(
                level, charging);

        // Fall back to the the coarse battery icons
        if (!icon_theme.has_icon(bat_icon.icon_name))
            bat_icon.icon_name = dev.icon_name;
    }

    // Greeter

    private void populate_users() {
        // Clear the user carousel
        var curr_index = (uint) user_carousel.position;
        while (user_carousel.n_pages > 0)
            user_carousel.remove(user_carousel.get_nth_page(0));

        // Repopulate the user carousel
        var user_list = LightDM.UserList.get_instance();
        foreach (var user in user_list.users)
            new Greeter.UserTile(user, user_carousel);
        //new Greeter.UserTile.guest(user_carousel); // TODO: Re-enable

        // Scroll back to where we were
        user_carousel.scroll_to(user_carousel.get_nth_page(curr_index), true);
    }

    private void on_user_progress_changed() {
        var progress = user_carousel.position;
        for (int i = 0; i < user_carousel.n_pages; i++) {
            var widget = user_carousel.get_nth_page(i);
            var distance = (progress - i).abs().clamp(0, 1);
            widget.opacity = 1 - (0.7 * distance);
        }
    }

    private void shake() {
        password_entry.add_css_class("error");
        password_box.add_css_class("shake");
    }

    private void set_password_visibility(bool visible) {
        if (visible) {
            password_entry.visible = true;
            login_button.icon_name = "go-next-symbolic";
        } else {
            password_entry.visible = false;
            login_button.label = "Log in";
        }
    }

    private void on_greeter_prompt(PromptType type) {
        switch (type) {
            case NONE:
                set_password_visibility(false);
                break;
            case PASSWORD:
            case NEW_PASSWORD: // TODO: Handle separately
                set_password_visibility(true);
                break;
            case PIN: // TODO: Handle these types
            case PATTERN:
            case FINGERPRINT:
            default:
                error("Unhandled greeter prompt type");
        }
    }

    private void on_greeter_failed() {
        on_greeter_complete();
        shake();
    }

    private void on_greeter_success() {
        on_greeter_complete();
        greeter.start_session();
    }

    private void on_greeter_complete() {
        password_box.sensitive = true;
    }

    [GtkCallback]
    private void on_selected_user_changed(uint index) {
        var user = user_carousel.get_nth_page(index) as Greeter.UserTile;
        greeter.preauth(user);

        focus_password_entry();
        password_entry.text = "";
        on_password_entry_changed(); // Ensure the shake gets cleared

        // TODO: Persist the selected user across reboots
    }

    [GtkCallback]
    private void on_password_entry_changed() {
        password_entry.remove_css_class("error");
        password_box.remove_css_class("shake");
    }

    [GtkCallback]
    private void on_login_button_clicked() {
        password_box.sensitive = false;
        greeter.auth(password_entry.text);
    }

    // Shield control

    [GtkCallback]
    private void hide_shield() {
        if (main_switcher.position == 0.0)
            main_switcher.scroll_to(main_switcher.get_nth_page(1), true);
    }

    private void on_shield_progress_changed() {
        var val = main_switcher.position;
        var css = @"* { background-color: alpha(@theme_bg_color, $val); }";
        bg_provider.load_from_data(css.data);
        bg_provider.gtk_private_changed();

        // Redirect focus to authentication as soon as we start scrolling
        if (val > 0.0) focus_password_entry();
    }

    private void focus_password_entry() {
        // Do nothing if we're on the shield page
        if (main_switcher.position == 0.0) return;

        // GtkPasswordEntry doesn't have this function delegated through :/
        ((Gtk.Text) password_entry.get_delegate()).grab_focus_without_selecting();
        this.set_default_widget(login_button);
    }

    [GtkCallback]
    private void on_main_switcher_changed(uint index) {
        var current = main_switcher.get_nth_page(index);
        var other = main_switcher.get_nth_page(1 - index);
        current.can_focus = true;
        other.can_focus = false;

        if (index == 0) {
            slide_arrow_btn.grab_focus();
            password_entry.text = "";
        } else {
            focus_password_entry();
        }
    }

    [GtkCallback]
    private bool on_key_down(Gtk.EventControllerKey key, uint val, uint code, Gdk.ModifierType state) {
        if (main_switcher.position == 0.0) {
            hide_shield();
            return Gdk.EVENT_STOP;
        }

        if (val == Gdk.Key.Escape) {
            main_switcher.scroll_to(main_switcher.get_nth_page(0), true);
            return Gdk.EVENT_STOP;
        }

        return Gdk.EVENT_PROPAGATE;
    }

    // Misc

    [GtkCallback]
    private void emergency_btn_clicked() {
        var dialog = new Gtk.MessageDialog(this, 0, Gtk.MessageType.ERROR,
            Gtk.ButtonsType.OK, "Emergency actions are not implemented yet");
        dialog.response.connect(dialog.destroy);
        dialog.show();
    }

    [GtkCallback]
    private void power_btn_clicked() {
        try {
            // TODO: Call dbus directly
            Process.spawn_command_line_sync("gde-end-session-dialog");
        } catch (Error e) {
            warning("Coudln't spawn end session dialog: %s", e.message);
        }
    }
}

