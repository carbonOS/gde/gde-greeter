enum Greeter.PromptType {
    NONE,
    PASSWORD,
    NEW_PASSWORD,
    PIN,
    NEW_PIN,
    PATTERN,
    FINGERPRINT;
}

class Greeter.GreeterImpl : Object {
    private LightDM.Greeter? greeter;
    private UserTile? user = null;
    private string? password = null;

    construct {
        // Connect to the greeter
        greeter = new LightDM.Greeter();
        try {
            greeter.connect_to_daemon_sync();
        } catch (Error e) {
            critical("Failed to connect to LightDM: %s", e.message);
            greeter = null;
        }

        // Hook up signals
        if (greeter != null) {
            greeter.authentication_complete.connect(on_auth_complete);
            greeter.show_message.connect(on_show_message);
            greeter.show_prompt.connect(on_show_prompt);
        }
    }

    private void start_auth_internal(UserTile user) {
        try {
            // Cancel any ongoing auth
            if (greeter.in_authentication) greeter.cancel_authentication();

            // Start new auth
            if (user.is_guest)
                greeter.authenticate_as_guest();
            else
                greeter.authenticate(user.username);
        } catch (Error e) {
            critical("Couldn't start authentication: %s", e.message);
            failed();
        }
    }

    // Select the user and ask LightDM for the prompt type
    public void preauth(UserTile user) {
        if (greeter == null) { // No-op impl
            prompt(!user.is_guest ? PromptType.PASSWORD : PromptType.NONE);
            return;
        }

        this.user = user;
        this.password = null;
        start_auth_internal(user);
    }

    // Authenticate the selected user
    public void auth(string? pass) {
        if (greeter == null) { // No-op impl
            if (pass == "password")
                success();
            else
                failed();
            return;
        }

        this.password = pass ?? "";
        if (greeter.is_authenticated)
            success(); // User didn't need a password. We're done
        else
            start_auth_internal(user); // User needs the password. Authenticate
    }

    // Start the selected user's session
    public void start_session() {
        if (greeter == null) // No-op impl
            Process.exit(0);

        try {
            greeter.start_session_sync("graphite");
        } catch (Error e) {
            critical("Failed to start session: %s", e.message);
            failed();
        }
    }

    private void on_auth_complete() {
        if (password == null)
            prompt(PromptType.NONE); // preauth + complete = no prompt
        else
            if (greeter.is_authenticated)
                success(); // complete + authenticated = sucess
            else
                failed(); // complete + !authenticated = failed
    }

    private void on_show_message(string text, LightDM.MessageType type) {
        if (type == LightDM.MessageType.ERROR)
            warning("LightDM ERROR: %s", text);
        else
            message("LightDM INFO: %s", text);
    }

    private void on_show_prompt(string text, LightDM.PromptType type) {
        message("LightDM Prompt: `%s`", text);
        if (password != null) {
            try {
                greeter.respond(password);
            } catch (Error e) {
                critical("Failed to respond to greeter prompt: %s", e.message);
                failed();
            }
        } else {
            var is_new = text.down().contains("new");
            if (text.down().contains("password")) {
                if (is_new)
                    prompt(PromptType.NEW_PASSWORD);
                else
                    prompt(PromptType.PASSWORD);
            } else if (text.down().contains("pin")) {
                if (is_new)
                    prompt(PromptType.NEW_PIN);
                else
                    prompt(PromptType.PIN);
            } else if (text.down().contains("pattern")) {
                prompt(PromptType.PATTERN);
            } else if (text.down().contains("finger")) {
                prompt(PromptType.FINGERPRINT);
            } else {
                prompt(PromptType.NONE);
                critical("Unhandled lightdm prompt: `%s`", text);
            }
        }
    }

    // Indicates the prompt type for the user. Emmitted during preauth
    public signal void prompt(PromptType type);

    // Login successfully completed
    public signal void success();

    // Login failed
    public signal void failed();
}
