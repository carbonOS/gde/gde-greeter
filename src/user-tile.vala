[GtkTemplate(ui="/sh/carbon/gde-greeter/ui/user.ui")]
class Greeter.UserTile : Gtk.Button {
    [GtkChild] unowned Adw.Avatar avatar;
    [GtkChild] unowned Gtk.Label user_name;
    [GtkChild] unowned Gtk.Image logged_in;

    public bool is_guest = false;
    public string username = "";

    private Adw.Carousel user_carousel;

    public UserTile(LightDM.User user, Adw.Carousel carousel) {
        user_carousel = carousel;
        carousel.append(this);

        username = user.name;
        user.changed.connect(() => {
            match_lightdm_user(user);
        });
        match_lightdm_user(user);
    }

    // We could do this in the .ui file via GtkExpression, however since we
    // want to support a guest case we need to do this manually
    private void match_lightdm_user(LightDM.User user) {
        // Load custom avatar, if available
        if (user.image != null) {
            try {
                var img_file = File.new_for_path(user.image);
                avatar.custom_image = Gdk.Texture.from_file(img_file);
            } catch (Error e) {
                warning("Failed to load user avatar: %s", e.message);
                avatar.custom_image = null;
            }
        } else avatar.custom_image = null;

        user_name.label = user.display_name;
        logged_in.visible = user.logged_in;
    }

    public UserTile.guest(Adw.Carousel carousel) {
        user_carousel = carousel;
        carousel.append(this);

        avatar.show_initials = false;
        avatar.icon_name = "incognito-symbolic";
        user_name.label = "Guest";
        logged_in.visible = false;
        is_guest = true;
    }

    [GtkCallback]
    private void on_clicked() {
        user_carousel.interactive = false;
        user_carousel.scroll_to(this, true);
        user_carousel.interactive = true;
    }

}
