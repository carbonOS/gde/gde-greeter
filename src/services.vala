public enum UPower.DeviceType {
	UNKNOWN = 0,
	BATTERY = 2,
	UPS = 3
}

public enum UPower.DeviceState {
	UNKNOWN = 0u,
	CHARGING = 1u,
	DISCHARGING = 2u,
	EMPTY = 3u,
	FULLY_CHARGED = 4u,
	PENDING_CHARGE = 5u,
	PENDING_DISCHARGE = 6u
}

public enum UPower.DeviceWarningLevel {
	UNKNOWN = 0,
	NONE = 1,
	DISCHARGING = 2,
	LOW = 3,
	CRITICAL = 4,
	ACTION = 5
}

[DBus (name = "org.freedesktop.UPower.Device")]
public interface UPower.Device : DBusProxy {
	public abstract uint Type { public owned get;}
	public abstract uint state { public owned get;}
	public abstract double percentage { public owned get;}
	public abstract double energy { public owned get;}
	public abstract double energy_full { public owned get;}
	public abstract double energy_rate { public owned get;}
	public abstract int64 time_to_empty { public owned get;}
	public abstract int64 time_to_full { public owned get;}
	public abstract bool is_present { public owned get;}
	public abstract string icon_name { public owned get;}
	public abstract uint warning_level { public owned get;}
	public abstract uint64 update_time { public owned get;public set;}
}
