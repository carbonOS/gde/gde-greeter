void main() {
    Gtk.init();
    Adw.init();
    Adw.StyleManager.get_default().color_scheme = Adw.ColorScheme.FORCE_DARK;
    var display = Gdk.Display.get_default();

    // Icon theme
    var icon_theme = Gtk.IconTheme.get_for_display(display);
    icon_theme.add_resource_path("/sh/carbon/gde-greeter/icons");

    // CSS theme
    var css_provider = new Gtk.CssProvider();
    css_provider.load_from_resource("/sh/carbon/gde-greeter/ui/greeter.css");
    Gtk.StyleContext.add_provider_for_display(display, css_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    // Start the window
    var win = new Greeter.Window();
    while (win.visible) MainContext.@default().iteration(true);
}

